const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("Hello Wolrd");
});

app.get("/api/course", (req, res) => {
  res.send([1, 2, 3, 4]);
});

app.get("/api/course/:id", (req, res) => {
  res.send(req.params.id);
});

//port
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listen on port ${port}.`));
// app.post();
// app.put();
// app.delete();
